```
 /$$    /$$ /$$$$$$$$ /$$   /$$ /$$$$$$$$ /$$$$$$   /$$$$$$ 
| $$   | $$| $$_____/| $$$ | $$|__  $$__//$$__  $$ /$$__  $$
| $$   | $$| $$      | $$$$| $$   | $$  | $$  \ $$| $$  \__/
|  $$ / $$/| $$$$$   | $$ $$ $$   | $$  | $$$$$$$$|  $$$$$$ 
 \  $$ $$/ | $$__/   | $$  $$$$   | $$  | $$__  $$ \____  $$
  \  $$$/  | $$      | $$\  $$$   | $$  | $$  | $$ /$$  \ $$
   \  $/   | $$$$$$$$| $$ \  $$   | $$  | $$  | $$|  $$$$$$/
    \_/    |________/|__/  \__/   |__/  |__/  |__/ \______/ 
```
### v2.2.0 (en desarrollo)
```
Mejora y nuevas funcionalidades:
[ ] productos: combos de productos, unidad de medida
[ ] compras: que se pueda establecer la localización del producto
[ ] pedidos de meseros
[ ] vista: con la organización de las mesas (parametrizable)
```
### v2.1.1 (2020-08-05)
```
Mejora módulos:
[x] compras: se agregó campo que permite agregar margen de ganancia en los productos comprados,
    porcentaje en el descuento
[x] productos: se agregó lista de entradas y salidas
[x] facturación: en el detalle se puede ir al producto y porcentaje en el descuento
[x] ventas: se agregó columna con el costo total de la factura 
[x] inventario: se cambió la importación y exportación de productos EXCEL (xlsx)
```
### v2.1.0 (2020-06-01)
```
Nuevos módulos:
[x] login: productos más vendidos
[x] pedidos: lista de productos, carrito de compras
[x] inventario: kardex, importación, exportación de los productos (CSV)
[x] catálogos: compañías (solo super admin)
[x] funcionalidades: ajuste por defecto a los usuarios
```
### v2.0.0 (2020-03-01)
```
Cambio a JAVA todo. Mejora del modelo
Módulos por defecto y mejoras sustanciales en:
[x] facturación
[x] productos
[x] compras
[x] bodegas
[x] reportes
[x] pedidos
[x] inventario: ajuste de entradas y salidas
[x] catálogos: usuarios, clientes, proveedores, marcas
```
### v1.0.0 (2018-11-01)
```
Módulos por defecto (desarrollado con PHP):
[x] facturación
[x] productos
[x] compras
[x] bodegas
[x] reportes
[x] pedidos
[x] inventario: ajuste de entradas y salidas
[x] catálogos: usuarios, clientes, proveedores, marcas
```
